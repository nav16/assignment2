//
//  SpaceshipScene.swift
//  AsteroidAvoider_Swift
//
//  Created by Ash Mishra on 2015-05-12.
//  Copyright (c) 2015 VFS. All rights reserved.
//

import Foundation
import SpriteKit
import CoreMotion

public class SpaceshipScene: SKScene, SKPhysicsContactDelegate {
    
    private var hitCount = 0
    
    // for motion control
    private let motionManager: CMMotionManager
    
    // state variables for moving the ship
    private var shipWidth: CGFloat = 0.0
    private var shipHeight: CGFloat = 0.0
    private var xMax:CGFloat = 0.0
    private var yMax:CGFloat = 0.0
    private var gameIsOver = false
    // contact bit masks.  << is left shift of base 2
    private let rockCategory: UInt32 = 0x1 << 1
    private let shipCategory: UInt32 = 0x1 << 2
    private let astronautCategory: UInt32 = 0x1 << 3
    
    
    override init(size: CGSize) {
        self.motionManager = CMMotionManager()
        super.init(size: size)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // START ADDING YOUR CODE HERE
    
    func initMotionManager() {
        
        self.motionManager.accelerometerUpdateInterval = 1/60.0
        
        var shipNode = self.childNodeWithName("spaceship")
        var timeNode = self.childNodeWithName("time") as! SKLabelNode
        
        var time = 0.0
        
        self.motionManager.startAccelerometerUpdatesToQueue( NSOperationQueue.currentQueue()!,
                                                             withHandler: { (accelerometerData, error) -> Void in
                                                                time = time + 0.01
                                                                timeNode.text = NSString(format: "%.1f", time) as String
                                                                var xDelta = CGFloat(accelerometerData!.acceleration.x * 5)
                                                                var xPosition = shipNode!.position.x
                                                                var newX = CGFloat(xDelta) + xPosition
                                                                if (newX < self.shipWidth || newX > self.xMax) {
                                                                    xDelta = 0
                                                                }
                                                                var yDelta = CGFloat(accelerometerData!.acceleration.y * 5)
                                                                var yPosition = CGFloat(shipNode!.position.y)
                                                                var newY = CGFloat(yDelta) + yPosition
                                                                if (newY < self.shipHeight || newY > self.yMax) { yDelta = 0
                                                                }
                                                                if (newX > 0 || newY > 0) {
                                                                    var moveShip = SKAction.moveByX(xDelta, y: yDelta, duration: 0)
                                                                    shipNode!.runAction(moveShip) }
        })
    }
    
    func addClock() {
        
        let timeNode = SKLabelNode(fontNamed: "Helvetica")
        timeNode.name = "time"
        timeNode.fontSize = 16
        timeNode.text = "0.0"  // initial time is zero
        timeNode.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        timeNode.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
        
        
        self.addChild(timeNode)
    }
    
    func engineFlare() -> SKEmitterNode {
        
        let burstPath = NSBundle.mainBundle().pathForResource("Burst", ofType: "sks")
        let burstEmitter = NSKeyedUnarchiver.unarchiveObjectWithFile(burstPath!) as! SKEmitterNode
        
        burstEmitter.emissionAngle = -1.571 // flip the particle to point downwards
        burstEmitter.particleSize = CGSizeMake(20, 100)  // define width and height
        burstEmitter.particlePositionRange = CGVectorMake(10, 0) // x and y variance of the particles generated
        
        burstEmitter.targetNode = self
        
        return burstEmitter
    }
    
    func gameOver() {
        
        self.removeAllActions()
        self.motionManager.stopAccelerometerUpdates()
        
        let gameOverNode = SKLabelNode(fontNamed: "Chalkduster")
        gameOverNode.name = "gameover"
        gameOverNode.text = "GAME OVER"
        gameOverNode.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
        gameOverNode.xScale = 0.1
        gameOverNode.yScale = 0.1
        gameOverNode.alpha = 0.0
        
        self.addChild(gameOverNode)
        
        
        let zoomInAction = SKAction.group([
            SKAction.fadeAlphaTo(1, duration: 0.25),
            SKAction.scaleXTo(1, y: 1, duration: 0.25)
            ]
        )
        
        let zoomOutAction = SKAction.group([
            SKAction.fadeAlphaTo(0.5, duration: 0.25),
            SKAction.scaleXTo(0.9, y: 0.9, duration: 0.25)
            ]
        )
        
        let gameOverAction = SKAction.repeatActionForever(
            SKAction.sequence([zoomInAction, zoomOutAction])
        )
        
        gameOverNode.runAction(gameOverAction)
    }
    
    // random math functions
    func skRandf() -> CGFloat {
        return CGFloat(rand()) / CGFloat(RAND_MAX)
    }
    
    func skRand(low: CGFloat, high: CGFloat) -> CGFloat {
        return skRandf() * (high-low) + low;
    }
    
    override public func didMoveToView (view: SKView)
    {
        self.initSceneContents()
        self.initMotionManager()
    }
    
    func initSceneContents() 	{
        self.backgroundColor = SKColor.blackColor()
        
        // set this scene as the delegate for contact hits
        self.physicsWorld.contactDelegate = self
        
        // add a spaceship to the middle of the scene
        self.addSpaceShip()
        
        // add the elapsed time clock
        self.addClock()
        
        // add the asteroids
        self.addRocks()
        
        self.addAstronauts()
    }
    
    // the sprite node for the spaceship and its lights
    func addSpaceShip()  {
        
        // add a texture for the spaceship
        var shipTexture = SKTexture(imageNamed:"rocket.png")
        var ship = SKSpriteNode(texture:shipTexture)
        
        // give the Node a name (so we can reference it later)
        ship.name = "spaceship"
        
        // rocket.png is not a retina graphic, let's scale it by 1/2
        ship.xScale = 0.5
        ship.yScale = 0.5
        // create a physics body for the ship using the texture
        ship.physicsBody  = SKPhysicsBody(texture:shipTexture, alphaThreshold: 0, size:ship.size);
        
        // don't apply world physics to the spaceship hull (i.e. no gravity, no torque)
        ship.physicsBody?.dynamic = false
        
        // assign a category to the ship's body in the physics simulator
        ship.physicsBody?.categoryBitMask = shipCategory
        // configure the ship's physics to allow collisions with rocks
        ship.physicsBody?.collisionBitMask = rockCategory
        // enable callbacks when the ship collides with a rock
        ship.physicsBody?.contactTestBitMask = rockCategory
        
        // create a dynamic engine flare for the ship
        let engineFlare = self.engineFlare()
        engineFlare.position = CGPointMake(0, -ship.size.height+30)
        ship.addChild(engineFlare)
        
        // center the ship in the scene
        ship.position = CGPointMake(CGRectGetMidX(self.frame),
                                    CGRectGetMidY(self.frame));
        // add the spaceship to the scene
        self.addChild(ship)
        
        // save global values for later reference
        self.shipWidth = ship.size.width;
        self.shipHeight = ship.size.height;
        self.xMax = self.frame.size.width - self.shipWidth;
        self.yMax = self.frame.size.height - self.shipHeight;
    }
    
    func addRocks() {
        
        // add rocks to the view
        var addRocksAction = SKAction.sequence(
            [   // a sequence is an array of actions
                SKAction.runBlock(self.addRockNode),
                SKAction.waitForDuration(0.15, withRange:0.15)
            ])
        
        // let's add rocks to the scene "forever"
        self.runAction(SKAction.repeatActionForever(addRocksAction));
    }
    
    func addRockNode()  {
        // create a rock – a simple square object
        let rock = SKSpriteNode(color:SKColor.brownColor(), size:CGSizeMake(8, 8))
        rock.position = CGPointMake(skRand(0, high: self.size.width),
                                    skRand(self.size.height-100, high: self.size.height))
        // give the node a name
        rock.name = "rock"
        
        // giving the rock a physics body allows the world to apply gravity and collisions
        rock.physicsBody = SKPhysicsBody(rectangleOfSize: rock.size)
        rock.physicsBody!.usesPreciseCollisionDetection = true
        
        // add the rock to the scene
        self.addChild(rock)
    }
    
    func addAstronauts(){
        var addAstronautsAction = SKAction.sequence(
            [   // a sequence is an array of actions
                SKAction.runBlock(self.addAstronautNode),
                SKAction.waitForDuration(0.15, withRange: 0.15)
            ]
        )
        // let's add astronauts to the scene "forever"
        self.runAction(SKAction.repeatActionForever(addAstronautsAction))
    }
    
    func addAstronautNode(){
        // add a texture for the spaceship
        var astronautTexture = SKTexture(imageNamed: "youthinkthisisagame.png")
        var astronaut = SKSpriteNode(texture: astronautTexture)
        // give the node a name
        astronaut.name = "astronaut"
        astronaut.xScale = 0.15
        astronaut.yScale = 0.15
        
        astronaut.position = CGPointMake(skRand(0,high: self.size.width),skRand(self.size.height-100,high:self.size.height))
        
        astronaut.physicsBody = SKPhysicsBody(rectangleOfSize: astronaut.size)
        astronaut.physicsBody!.usesPreciseCollisionDetection = true
        // assign a category to the ship's body in the physics simulator
        //astronaut.physicsBody?.categoryBitMask = astronautCategory
        self.addChild(astronaut)
    }

    override public func didSimulatePhysics() {
        
        // iterate over the rocks in the scene, and delete those that are now offscreen
        self.enumerateChildNodesWithName("rock") { node, stop in
            if node.position.y<0 {
                node.removeFromParent()
            }
        }
        
        self.enumerateChildNodesWithName("astronaut") { node, stop in
            if node.position.y<0 {
                node.removeFromParent()
            }
        }
    }
    
    public func didBeginContact(contact: SKPhysicsContact) {
        let MAX_HIT_COUNT = 100;
        var shipPhysicsBody: SKPhysicsBody
        
        if (contact.bodyA.categoryBitMask == shipCategory)
        {
            shipPhysicsBody = contact.bodyA
            if(contact.bodyB.node?.name == "astronaut")
            {
                print("astronaut")
                self.hitCount--
            }else{
                //Show damage on the ship
                var ship = shipPhysicsBody.node as! SKSpriteNode
                
                //*
                var addDamage = SKAction.colorizeWithColor(SKColor.redColor(),
                                                           colorBlendFactor:CGFloat(self.hitCount/MAX_HIT_COUNT), duration:0)
                ship.runAction(addDamage)
                self.hitCount++;
                print("rock")
                }
        }
        else {
            shipPhysicsBody = contact.bodyB
            if(contact.bodyB.node?.name == "astronaut")
            {
                self.hitCount--
                print("astronaut")
            }else{
                //Show damage on the ship
                var ship = shipPhysicsBody.node as! SKSpriteNode
                
                //*
                var addDamage = SKAction.colorizeWithColor(SKColor.redColor(),
                                                           colorBlendFactor:CGFloat(self.hitCount/MAX_HIT_COUNT), duration:0)
                ship.runAction(addDamage)
                self.hitCount++;
                print("rock")
            }
            
            

        }
        //print(self.hitCount)
        
        // # of times the rocks hit the ship
        if self.hitCount==MAX_HIT_COUNT {
            self.gameOver()
        }
    }
    public override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if (gameIsOver == true){
            var transitionReset:SKTransition = SKTransition.fadeWithDuration(1)
            var scene:SKScene = HelloScene(size: self.size)
            self.view?.presentScene(scene, transition: transitionReset)
            
        }
        
        
    }}